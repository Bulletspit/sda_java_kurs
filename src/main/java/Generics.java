import com.google.common.collect.Lists;

import java.util.List;

public class Generics {
    public static void main(String[] args) {
        List<String> myListoOfRandomWords = Lists.newArrayList("Dzidzia", "Kmieciu", "Król", "Autobus");
        List<Integer> myListOfSomeNumbers = Lists.newArrayList(5, 8, 1, 8, 124, 8, 2, 7, 2, 42);
        printMethod(myListoOfRandomWords);
        printSumOfIntsMethodStreamStyle(myListOfSomeNumbers);
    }


    public static <E> void printMethod(List<E> listToPut) {
        for (E e : listToPut) {
            System.out.println(e);
        }
    }


    public static <E> void printSumOfIntsMethodStreamStyle(List<E> listToPut) {
        Integer sum = listToPut.stream().mapToInt(Object::hashCode).sum();
        System.out.println("suma elementów z listy to: " + sum);
    }
}
